/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcv;

/**
 *
 * @author YILMAZER
 */
public class arabacontroller {
    private araba model;
    private arabaview view;
    public arabacontroller(araba model, arabaview view)
    {
        this.model=model;
        this.view=view;
    }

    public String getArabaModel()
    {
        return model.getmodel();
    }
    public void setArabaModel(String model1)
    {
        model.setmodel(model1);
    }
    public String getArabaNo(String no)
    {
        return model.getno();
    }
    public void setArabaNo(String no)
    {
        model.setno(no);
    }
    public void guncel()
    {
        view.goster(model.getno(), model.getmodel());
    }
}
